# frozen_string_literal: true

load 'vendor/bundle/bundler/setup.rb'

require 'telegram/bot'
require 'telegram/bot/types'
require 'json'

Telegram.bots_config = {
  default: ENV['TG_TOKEN']
}

def hello(event:, context:)
  body = JSON.parse(event['body'])
  update = Telegram::Bot::Types::Update.new(body)
  if update.message
    answer_message(update.message)
  elsif update.inline_query
    answer_inline_query(update.inline_query)
  end
  { statusCode: 200 }
end

def answer_message(message)
  p 'answering message'
rescue Telegram::Bot::Error => e
  p e
end

def answer_inline_query(query)
  p 'answering inline query'
  return if query.query.nil? || query.query.empty?
rescue Telegram::Bot::Error => e
  p e
end
