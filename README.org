* AWS Serverless Templates for Telegram Bot with Ruby

** Usage

Create new serverless project.

#+BEGIN_SRC shell
sls create --template-url https://gitlab.com/merrickluo/aws-telegram-bot --path $PROJECT_NAME
#+END_SRC

Install dependencies.

#+BEGIN_SRC shell
scripts/install-deps.sh
#+END_SRC

Setup environment variables.

#+BEGIN_SRC shell
export DEV_TG_TOKEN="token for dev stage deployment"
export PROD_TG_TOKEN="token for prod stage deployment"
#+END_SRC

And dont forget aws credentials mentioned in serverless documentation.

After coding, you can setup bot webhook using this little helper with.

#+BEGIN_SRC shell
./scripts/setup_webhook.sh $DEV_TG_TOKEN $URL_RETURNED_BY_DEPLOY
#+END_SRC

This template use [[https://github.com/telegram-bot-rb/telegram-bot][telegram-bot]] and [[https://github.com/telegram-bot-rb/telegram-bot-types][telegram-bot-types]] for bot interaction and data parsing, and [[https://github.com/joshuaflanagan/serverless-ruby-package][serverless-ruby-package]] for package gems.

Also don't forget to checkout [[https://serverless.com/framework/docs/][serverless documentions]].
